# -*- coding: utf-8 -*-
from abc import ABCMeta,abstractmethod
import pprint
import bamboo

class Data(object):
    __metaclass__ = ABCMeta

    def __init__(self, jsonData):
        self._jsonData = jsonData
        self._completd = False

    @abstractmethod
    def do_complete(self):
        pass

    @property
    def completed(self): return self._completd

    def complete(self):
        if not self.completed:
            self._completd = True
            self.do_complete()

    def __getitem__(self,keyPath):
        keys = keyPath.split('.')
        obj = self._jsonData[keys[0]]
        for key in keys[1:]:
            obj = obj[key]
        return obj

    def get(self,keyPath,value=None):
        try:
            return self.__getitem__(keyPath)
        except:
            return value

    def has(self,keyPath):
        try:
            self.__getitem__(keyPath)
            return True
        except:
            return False

    def pprint(self):
        pprint.pprint(self._jsonData)

    @property
    def key(self): return self.get('key')
    @property
    def name(self): return self.get('shortName')
    @property
    def url(self): return self.get('link.href')


class Project(Data):
    def __init__(self,jsonData):
        super(Project,self).__init__(jsonData)
        self._plans = []
        for p in self.get('plans.plan',[]):
            self.addPlan(Plan(p))

    def addPlan(self,plan):
        if not plan in self._plans:
            self._plans.append(plan)
        plan.setProject(self)

    @property
    def plans(self):
        return self._plans

    def __str__(self):
        return "Project({0})".format(self.key)

    def do_complete(self):
        pass

    @property
    def name(self): return self.get('name')


class Plan(Data):
    def __init__(self,jsonData):
        super(Plan,self).__init__(jsonData)
        self._project = None
        self._stages = []
        for s in self.get('stages.stage',[]):
            self._stages.append(Stage(s))
        self._branches = []
        for b in self.get('branches.branch',[]):
            self._branches.append(Branch(b,self))
        self._results = []

    def setProject(self, project):
        if not self._project:
            self._project = project

    @property
    def project(self): return self._project

    def addStage(self):
        pass

    def do_complete(self):
        self._stages[:] = []
        self._jsonData = bamboo.GetAsJSON('plan/' + self.key, 'stages.stage.plans.plan.branches.branch')
        for s in self.get('stages.stage',[]):
            self._stages.append(Stage(s))
        self._results = bamboo.getHistory(self.key)

    @property
    def results(self): return self._results

    @property
    def branches(self): return self._branches

    def jobs(self):
        for stage in self._stages:
            for job in stage._jobs:
                yield job

class Branch(Plan):
    def __init__(self,jsonData,masterPlan):
        super(Branch,self).__init__(jsonData)
        self._masterPlan = masterPlan
    def do_complete(self):
        pass
    @property
    def name(self): return self._masterPlan.name + '(' + super(Branch,self).name + ')'


class Stage(Data):
    def __init__(self,jsonData):
        super(Stage,self).__init__(jsonData)
        self._plan = None
        self._jobs = []
        for j in self.get('plans.plan',[]):
            self._jobs.append(Job(j))

    def setPlan(self,plan):
        if not self._plan:
            self._plan = plan

    def do_complete(self):
        pass

    @property
    def plan(self): return self._plan

    def addJOb(self, job):
        if not job in self._jobs:
            self._jobs.append(job)
            job.setStage(self)


class Job(Data):
    def __init__(self,jsonData):
        super(Job,self).__init__(jsonData)
        self._stage = None

    def setStage(self,stage):
        if not self._stage:
            self._stage = stage

    def do_complete(self):
        pass

    @property
    def stage(self): return self._stage


class History(Data):
    def __init__(self,jsonData):
        super(History,self).__init__(jsonData)
        self._builds = []
        for r in self.get('results.result',[]):
            self._builds.append(Build(r))

    def do_complete(self):
        pass

    @property
    def latestBuild(self):
        return self._builds[0]

class Build(Data):
    def __init__(self,jsonData):
        super(Build,self).__init__(jsonData)
        self._plan = None
        self._artifacts = []
        for a in self.get('artifacts.artifact',[]):
            self._artifacts.append(Artifact(a))

    def setPlan(self,plan):
        if not self._plan:
            self._plan = plan

    @property
    def plan(self): return self._plan

    def do_complete(self):
        pass

    def addArtifact(self, artifact):
        if not artifact in self._artifacts:
            self._artifacts.append(artifact)
            artifact.setBuild(self)

    @property
    def number(self): return self['buildNumber']

class Artifact(Data):
    def __init__(self,jsonData):
        super(Artifact,self).__init__(jsonData)
        self._result = None

    def setBuild(self,result):
        if not self._result:
            self._result = result
    @property
    def result(self): return self._result

    @property
    def url(self): return self.get('link.href')

