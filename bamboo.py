# -*- coding: utf-8 -*-
import httplib
from base64 import b64encode
import json
import data

def _getConnection():
    return httplib.HTTPConnection("autobamboo.uievolution.com")

def Get(url):
    '''
    returns httplib.HTTPResponse instance
    '''
    c = _getConnection()
#    c.set_debuglevel(8)
    userAndPass = b64encode(b"jtachikawa:xmintjun").decode("ascii")
    c.request('GET',
              url,
              headers={ 'Authorization' : 'Basic %s' %  userAndPass })
    return c.getresponse()

jdecoder = json.JSONDecoder()

def GetAsJSON(url, expand='', api_path='/rest/api/latest/'):
    resp = Get(api_path + url + '.json' + ('?expand=' + expand if expand else ''))
    return jdecoder.decode(resp.read())

def getLatestBuild(planKey):
    jsonData = GetAsJSON('result/' + planKey, 'results[0]')
    try:
        return jsonData['results']['result'][0]['buildNumber']
    except:
        import pprint,sys
        pprint.pprint(jsonData['results']['result'])
        #sys.exit()
        return None

def GetProjects(projKey=''):
    '''
    return { 'name'         : project name
             'key'          : project key
             'url'          : url
             'plans'        : list of plan dict }
    plan dict = {
        'name'     : plan name
        'key'      : plan key string
        'branches' : list of branche dict
    }
    branch dist = {
        'name' : branch name
        'key'  : branch kye string
    }
    '''
    jsonData = GetAsJSON('project', 'projects.project.plans.plan.branches')
    result = []
    for projDict in jsonData['projects']['project']:
        project = {'name': projDict['name'],
                   'key': projDict['key'],
                   'url': projDict['link']['href']}
        plans = []
        for plan in projDict['plans']['plan']:
            branches = []
            for branch in plan['branches']['branch']:
                branches.append({'name': branch['shortName'],
                                 'key':  branch['key'],
                                 'latest-build': getLatestBuild(branch['key'])})
            plans.append({'name': plan['shortName'],
                          'key' : plan['key'],
                          'latest-build': getLatestBuild(plan['key']),
                          'branches': branches})
        project['plans'] = plans
        result.append(project)
    return result

def getProjects():
    jsonData = GetAsJSON('project', 'projects.project.plans.plan.branches.branch,projects.project.plans.plan.stages.stage.plans.plan')
    for projDict in jsonData['projects']['project']:
        yield data.Project(projDict)

def getHistory(planKey):
    return data.History(GetAsJSON('result/' + planKey,
                                  'results.result.stages.stage.results.result'))

# def GetPlans(key):
#     jsonData = GetAsJSON('plan/' + key, 'stages.stage.plans.plan,branches.branch')
#     result = []
#     for stageDict in jsonData['stages']['stage']:
#         stage = {'name': stageDict['name'],
#                  'key': stageDict['key'],
#                  'url': stageDict['link']['href'],
#                  'latest-buid': getLatestBuild(key)}
#         stages = []
#         for stage in planDict['stages']['stage']:
#             stages.append({'name': stage['shortName'],
#                            'key' : stage['key'],


