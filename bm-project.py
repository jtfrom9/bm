# -*- coding: utf-8 -*-
import bamboo
import pprint
import sys

for proj in bamboo.getProjects():
    print(proj.name)
    #print('{0} ({1})'.format(proj.name, ",".join(plan.name for plan in proj.plans)))
    for plan in proj.plans:
        print('    {0} ({1}) [{2}]'.format(plan.name,
                                           plan.key,
                                           ",".join(j.key for j in plan.jobs())))
        for branch in plan.branches:
            print('    {0} ({1})'.format(branch.name,branch.key))
    #proj.plans[-1].complete()
    #proj.plans[-1].results.latest.pprint()
    # try:
    #     print(proj.plans[-1].results.latest.buildNumber)
    # except:
    #     print('except')
    # try:
    #     proj.plans[-1].pprint()
    # except:
    #     pass

sys.exit()

for proj in bamboo.GetProjects():
    print('{0} ({1})'.format(proj['name'],proj['key']))
    def tuppleOfNameAndKeysOfAllPlansAndItsBranches():
        for plan in proj['plans']:
            yield (plan['name'], plan['latest-build'], plan['key'])
            for branch in plan['branches']:
                yield ( plan['name'] + ' (' + branch['name'] + ')',
                        plan['latest-build'],
                        branch['key'] )
    listOfNameBuildKeys = list(tuppleOfNameAndKeysOfAllPlansAndItsBranches())
    planFmtString   ='  {{0:{0}}}  #{{1}}   ({{2}})'.format(max(len(name) for name, build, key in listOfNameBuildKeys))
    for name,build,key in listOfNameBuildKeys:
        print(planFmtString.format(name,build,key))


