# -*- coding: utf-8 -*-
import sys
import bamboo
import xmlpp

dumpFile = False

def test_get(api,fname):
    print(">> test_get: {0}\n".format(api))
    resp = bamboo.Get(api)
    print("resp: {0} {1}".format(resp.status, resp.reason))
    data = resp.read()
    xmlpp.pprint(data)
    if dumpFile:
        with open(fname,'w') as f:
            xmlpp.pprint(data,output=f)

if __name__=='__main__':
    if len(sys.argv) > 1:
        if sys.argv[1]=='dump':
            dumpFile = True
    # project
    test_get('/rest/api/latest/project/',"project.xml")
    test_get('/rest/api/latest/project?expand=projects.project',"project-projects.project.xml")
    test_get('/rest/api/latest/project?expand=projects.project.plans',"project-projects.project.plans.xml")
    # test_get('/rest/api/latest/project?expand=projects.project.plans.plan',"project-projects.project.plans.plan.xml")
    # test_get('/rest/api/latest/project?expand=projects.project.plans.plan.actions',"project-projects.project.plans.plan.actions.xml")
    # # plan
    test_get('/rest/api/latest/plan?max-result=100','plan.xml')
    # test_get('/rest/api/latest/plan?expand=plans.plan','plans.plan.xml')
    # test_get('/rest/api/latest/plan?expand=plans.plan.actions','plans.plan.actions.xml')

    # # result/SAB
    # test_get('/rest/api/latest/result/SAB','result.SAB.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM','result.SAB-AUTARM.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?expand=results','result.SAB-AUTARM.results.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?expand=results.result','result.SAB-AUTARM.results.result.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?expand=results[0-9].result','result.SAB-AUTARM.results0-9.result.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?expand=results.result&max-result=150','result.SAB-AUTARM.results.all.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?expand=results.result.artifacts&max-result=150','result.SAB-AUTARM.results.all.artifacts.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM/197?expand=results.result.artifacts','result.SAB-AUTARM.results.197.xml')
    test_get('/rest/api/latest/result/SAB-AUTARM-197?expand=stages.stage.results.result.artifacts','result.SAB-AUTARM.results.197.artifact.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?expand=results[0].result','result.SAB-AUTARM-latest.xml')
    # test_get('/rest/api/latest/result/SAB-AUTARM?max-result=100','result.SAB-AUTARM-100t.xml')

    test_get('/rest/api/latest/build','build.xml')
