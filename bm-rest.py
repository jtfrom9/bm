# -*- coding: utf-8 -*-
import bamboo
import argparse
import xmlpp
import json
import pprint

def argParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('uri',
                        metavar='URI',
                        type=str,
                        help='bamboo rest api')
    parser.add_argument('expand',
                        type=str,
                        nargs='?',
                        help='expand')
    parser.add_argument('--xml',
                        action='store_true',
                        help='use xml')
    return parser.parse_args()

jdecoder = json.JSONDecoder()

def jsonpprint(s):
    pprint.pprint(jdecoder.decode(s))
    # print(type(obj['stages']))
    # for r in obj['stages']['stage'][0]['results']['result']:
    #     for a in r['artifacts']['artifact']:
    #         print(a['name'])
#    for stage in obj['stages']:

def main():
    argp = argParser()
    expand = ''
    if argp.expand:
        expand = "?expand=" + argp.expand
    resp = bamboo.Get('/rest/api/latest/' + argp.uri + ("" if argp.xml else '.json') + expand)
    if argp.xml:
        xmlpp.pprint(resp.read())
    else:
        jsonpprint(resp.read())

if __name__=='__main__':
    main()

